import cv2

# objeto de conexão com a câmera
VIDEO_PATH = 'video.mp4'
video = cv2.VideoCapture(VIDEO_PATH)


# Capturando uma imagem da camera
# retorna status e um frame, python pode retornar mais de um objeto
ret, frame = video.read()


# exibindo uma janela com o frame
# exibe porem encerra logo após
# cv2.imshow('Nome da janela', frame)
# para não encerrar, podemos ficar aguardando um evento de teclado logo após exibir

# A execução vai parar aqui até acontecer o evento esperado
# o parametro é o tempo que vai esperar, 0 significa para sempre
# cv2.waitKey(0)


# Para video:
# loop infinito
while video.isOpened():

    # leitura de frame a cada ciclo do while
    ret, frame = video.read()

    # se o retorno da camera for valido
    if ret:
        # exibe em uma janela
        cv2.imshow('Nome da janela', frame)

    else:
        break

    # Identifica a tecla pressionada
    # o ord() é pra não precisar saber o código da tecla, pode passar o nome da tecla
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break