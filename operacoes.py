import cv2
import imutils

IMAGE_PATH = 'image.jpg'

frame = cv2.imread(IMAGE_PATH)

cv2.imshow('Imagem', frame)
cv2.waitKey(0)


resized = imutils.resize(frame, width=600)

cv2.imshow('resized', resized)
cv2.waitKey(0)

rotated = imutils.rotate_bound(frame, angle=90)

cv2.imshow('rotated', rotated)
cv2.waitKey(0)

cv2.imwrite('test.jpg', rotated)