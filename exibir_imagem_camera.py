import cv2

ID_CAMERA = 2

camera = cv2.VideoCapture(ID_CAMERA)


# Para video:
# loop infinito
while camera.isOpened():

    # leitura de frame a cada ciclo do while
    ret, frame = camera.read()

    # se o retorno da camera for valido
    if ret:
        # exibe em uma janela
        cv2.imshow('Nome da janela', frame)

    else:
        break

    # Identifica a tecla pressionada
    # o ord() é pra não precisar saber o código da tecla, pode passar o nome da tecla
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
