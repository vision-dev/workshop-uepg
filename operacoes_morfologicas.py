import cv2

# lendo a imagem
rice_image = cv2.imread('arroz.bmp')
cv2.imshow('image', rice_image)
print(rice_image.shape)

# convertendo pra escala de cinza
gray = cv2.cvtColor(rice_image, cv2.COLOR_BGR2GRAY)
cv2.imshow('gray', gray)
print(gray.shape)

copia = gray.copy()

copia = gray

# segmentação

binary = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 51, -40)
cv2.imshow('binary', binary)



# operações morfológicas

closed = cv2.morphologyEx(binary, cv2.MORPH_CLOSE, (3, 3), iterations=1)
cv2.imshow('closed', closed)




contours, _ = cv2.findContours(closed, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
cv2.drawContours(copia, contours, -1, (0, 255, 0), 1)

cv2.imshow('contornos', copia)

cv2.waitKey(0)