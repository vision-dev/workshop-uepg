import cv2

IMAGE_PATH = 'image.jpg'

frame = cv2.imread(IMAGE_PATH)

print(frame)

# exibindo uma janela com o frame
# encerra logo após exibir 
cv2.imshow('Nome da janela', frame)

# aguardando evento de teclado
cv2.waitKey(0)